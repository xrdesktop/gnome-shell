#!/usr/bin/env python3

available_methods = [ "enable", "disable" ]

import sys
if len(sys.argv) < 2 or (not sys.argv[1] in available_methods):
    print("Usage: {} [enable|disable]".format(sys.argv[0]))
    sys.exit()
method = sys.argv[1]

from gi.repository import Gio, GLib

bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
proxy = Gio.DBusProxy.new_sync(bus,
                               Gio.DBusProxyFlags.DO_NOT_AUTO_START,
                               None,
                               'org.gnome.Shell.XR',
                               '/org/gnome/Shell/XR',
                               'org.gnome.Shell.XR',
                               None)

is_enabled = proxy.get_cached_property("enabled")

# print("Is:", is_enabled)

prop_proxy = Gio.DBusProxy.new_sync(bus,
                                    Gio.DBusProxyFlags.DO_NOT_AUTO_START,
                                    None,
                                    'org.gnome.Shell.XR',
                                    '/org/gnome/Shell/XR',
                                    'org.freedesktop.DBus.Properties',
                                    None)

to_set = method == "enable"

# print("Set to:", to_set)

set_variant = GLib.Variant('(ssv)',("org.gnome.Shell.XR", "enabled",
                           GLib.Variant.new_boolean(to_set)))

prop_proxy.call_sync("Set", set_variant, Gio.DBusCallFlags.NONE, -1, None)

get_variant = GLib.Variant('(ss)',("org.gnome.Shell.XR", "enabled"))

res = prop_proxy.call_sync("Get", get_variant, Gio.DBusCallFlags.NONE, -1, None)

# print ("Is:", res)
